import * as React from 'react';
import './homePage.css';
import { Game } from '../game/game';

export class HomePage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    
    this.state = this.initialState;
  }

  render() {
    return (
      <div>
        <Game />
      </div>
    );
  }

  get initialState(): State {
    return {};
  }
}

interface Props {

}

interface State {

}