import * as React from 'react';

export class ViewPort extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    
  }

  render() {
    return (
      <canvas width={this.props.width} height={this.props.height} className="viewport" ref={(c: HTMLCanvasElement) => this.state. = c.getContext('2d')} />
    );
  }
}

interface Props {
  width: number;
  height: number;
}

interface State {
  ctx: Canvas2DContextAttributes
}