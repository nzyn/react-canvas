import * as React from 'react';
import './App.css';
import { Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { HomePage } from './components/homePage/homePage';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <main>
          <BrowserRouter>
            <Route component={HomePage} />
          </BrowserRouter>
        </main>
      </div>
    );
  }
}

export default App;
