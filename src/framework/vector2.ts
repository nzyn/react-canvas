export class Vector2 {
  constructor(
    public X: number,
    public Y: number
  ) {
    
  }

  get Zero(): Vector2 {
    return new Vector2(0, 0);
  }

  get One(): Vector2 {
    return new Vector2(1, 1);
  }
}